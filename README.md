# Processus de traitement des guides PSE CRf

Ce guide explique comment traite les guides de PSE émit par la Croix-Rouge française.

Les objectifs sont les suivants:

* Obtenir un PDF unique, avec une table des matière cliquable.
* Obtenir un PDF unique, avec table des matière, optimisé pour mobile.
* Diviser le PDF unique, en fichiers, pour chaque fiche ou section.

## Avant de commencer

Les instructions sont écritent pour Debian 10. Si vous utilisez un système d'exploitation différent, elles peuvent différer.

Les scripts sont en bash `#!/bin/bash`, donc à copier directement dans votre terminal.

Ces instructions concernent l'édition 2018 du guide. Donc, à adapater lors des révisions à venir.

Il est aussi important de noter que tout traitement avec un fichier PDF de 700 pages est assez long. Les opérations à ce stade prendronts plusieurs minutes pour compléter.

## Installer les dépendences

```
su -l root
apt update
apt install pdftk texlive-extra-utils
exit
```

## Obtenir les fichiers de départ

Il vous faudra obtenir la dernière versions (en PDF) de ces documents:

* Permiers secours en equipe: guide pratique
* II - Anatomie et pshysiologie
* III - Bilan
* IV - Conduite a tenir
* V - Techniques et materiels

## Préparer les documents

Renomer les documents selon l'ordre ci-haut:

```
1a.pdf
1b.pdf
1c.pdf
1d.pdf
1e.pdf
```

## Supprimer les pages innutiles

Afin de sauver du temps lors de l'utilisations, nous supprimons les pages blanches et le pages de notes destinées à l'impression.

```
function rmp () {
    in=$1; out=$2; sp=$3; ep=$4; rmp=$5;
    rmr=$(seq -s' ' ${sp} ${ep}|sed -E "s/ (${rmp}) / /g")
    pdftk ${in} cat ${rmr} output ${out}
}
```

Ensuite, il faut voir quels pages doivent être enlevées.

```
rmp 1a.pdf 2a.pdf 1 38 '2|18|32' 
rmp 1b.pdf 2b.pdf 1 23 '2|12|16|20'
rmp 1c.pdf 2c.pdf 1 27 '2|8|14|22'
rmp 1d.pdf 2d.pdf 1 257 '2|4|8|16|24|30|34|40|47|52|56|62|70|74|80|94|98|100|106|126|138|142|148|154|166|178|186|190|214|216|220|230|232|240|242' 
rmp 1e.pdf 2e.pdf 1 412 '2|4|18|24|28|38|40|50|56|64|66|70|90|98|112|116|120|124|134|146|158|164|168|172|174|178|186|188|196|200|204|208|230|232|236|242|258|266|284|290|304|308|314|322|328|348|352|356|360|362|372|384|388|394|398|400|404|410'
```

## Couper les bordures pour mobiles

L'ordre pour `--papersize` est gauche, bas, droit, haut. À noter que, changer n'importe quel paramètre affecte toute la mise en page. Il faut essayer pour voir le résultat. Çela vous prendra quelques tentatives si vous faites des modifications.

```
pdfjam 2a.pdf --papersize '{210mm,280mm}' --trim '18mm 25mm 20mm 12mm' --outfile 3a.pdf
pdfjam 2b.pdf --papersize '{210mm,280mm}' --trim '18mm 25mm 20mm 12mm' --outfile 3b.pdf
pdfjam 2c.pdf --papersize '{210mm,280mm}' --trim '18mm 25mm 20mm 12mm' --outfile 3c.pdf
pdfjam 2d.pdf --papersize '{210mm,280mm}' --trim '18mm 25mm 20mm 12mm' --outfile 3d.pdf
pdfjam 2e.pdf --papersize '{210mm,280mm}' --trim '18mm 25mm 20mm 12mm' --outfile 3e.pdf
```

## Combiner les fichiers

```
function combine () {
    in=$1; out=$2;
    files=$(find . -name "${in}"|sort|perl -p -e 's/\R/ /g')
    pdftk ${files} cat output ${out}
}
```

```
combine '2*.pdf' '4o.pdf'
combine '3*.pdf' '4m.pdf'
```

## Créer la table des matières

### Structure de la table

Une fois que vous avez le fichier 4o.pdf, vous pouvez créer la table des matière. Cette partie est très manuelle et plutôt longue. Elle peut prendre quelques heures et vous **ne devez pas faire d'erreurs**.

Le fichier [struct](./struct.csv) comprend 4 colones: page de début, page de fin, niveau du sommaire, titre.

Vous devrez modifier et recréer ce fichiers lors de la publication des nouvelles éditions.

Voici comment j'ai procédé pour le creer.

* J'ai copier/coller le glossaire
* J'ai noté le niveau d'indexe
* J'ai passé le document en revu, notant les numéros de pages
* J'ai utiliser Libre Office calc pour faire les maths des fins de pages

### Générer le fichiers de signet

```
gen_marks () {
    marks=$1; struct=$2;
    : > ${marks}

    while IFS="" read -r p || [ -n "$p" ]
    do
        page=$(printf '%s\n' "$p"|cut -d, -f1)
        level=$(printf '%s\n' "$p"|cut -d, -f3)
        title=$(printf '%s\n' "$p"|cut -d, -f4)

        printf "BookmarkBegin\nBookmarkTitle: ${title}\nBookmarkLevel: ${level}\nBookmarkPageNumber: ${page}\n" >> ${marks}
    done < ${struct}
}
```

```
gen_marks bm_compiled.txt struct.csv
```

```
add_marks () {
    in=$1; out=$2; marks=$3;
    pdftk ${in} update_info ${marks} output ${out}
}
```

```
add_marks 4o.pdf 5o.pdf bm_compiled.txt
add_marks 4m.pdf 5m.pdf bm_compiled.txt
```

## Séparer en fichiers par chapitre

**Attention**: Cela prend plusieurs heures!

```
mkdir fiches/

IFS=$'\n'
for line in $(cat struct.csv); do

    fp=$(echo $line|cut -d',' -f1)
    lp=$(echo $line|cut -d',' -f2)
    title=$(echo $line|cut -d',' -f4|tr -d '\r')

    in=4o.pdf
    out="fiches/${title}.pdf"
    range="${fp}-${lp}"
    echo "$in $range $out"

    pdftk ${in} cat ${range} output "${out}"

done
```

## Netoyage

Et pour terminer :)

```
mv 5o.pdf crf_pse_guide_originale.pdf
mv 5m.pdf crf_pse_guide_mobile.pdf
find . -name '[2-5]*.pdf' -delete
```
